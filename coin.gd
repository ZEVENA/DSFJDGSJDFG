extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var anim = get_node("AnimationPlayer").get_animation("Default")
	anim.set_loop(true)
	get_node("AnimationPlayer").play("Default")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_coin_body_shape_entered(body_id, body, body_shape, area_shape):
	var player = get_node('../../Player')
	player.coins += 1
	queue_free()
