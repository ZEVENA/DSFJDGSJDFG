extends KinematicBody2D


var velocity = Vector2.ZERO
var speed = 100


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func _physics_process(delta):
	var player = get_node('../../Player')
#	velocity = position.direction_to(player.position) * speed
	velocity = (player.position - position).normalized() * speed
	velocity = move_and_slide(velocity)

