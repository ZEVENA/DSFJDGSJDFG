extends KinematicBody2D

export (int) var speed = 150
export (int) var jump_speed = -180
export (int) var gravity = 300
var coins = 0
var jump = 0

var velocity = Vector2.ZERO

func get_input():
	velocity.x = 0
	if Input.is_action_pressed("ui_right"):
		velocity.x += speed
	if Input.is_action_pressed("ui_left"):
		velocity.x -= speed

func _physics_process(delta):
	get_input()
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	if Input.is_action_just_pressed("ui_accept") or Input.is_action_just_pressed('ui_up'):
		if is_on_floor() and jump == 0:
			jump += 1
			velocity.y = jump_speed
		elif jump == 1:
			velocity.y = jump_speed
			jump = 0


